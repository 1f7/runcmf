<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunCMF;

use RunCMF\Core\Menu\MenuCollection;
use RunCMF\Middlewares\CoreMiddleware;

class Init
{
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function init()
    {
        $this->registerMiddlewares();
//        $this->registerAdminRoute();
    }

    public function getModuleName()
    {
        return 'System';
    }

    public function getModuleAccessor()
    {
        return 'system';
    }

    public static function getAdminUrl()
    {
        return '/admin';
    }

    public function registerUserMenu()
    {
        $v = $this->app->getContainer()->get('view');
        return $v->fetch('menuUser.html.twig');
    }

    public static function registerAdminMenu(MenuCollection $menu, $url='')
    {
        $sysMenu = $menu->createItem('system', array(
            'label' => 'System',
            'icon'  => 'cogs',
            'url'   => '#'
        ));
        $sysMenu->setAttribute('class', 'nav nav-second-level');

        $dashboardMenu = $menu->createItem('sys-dashboard', array(
            'label' => 'Index',
            'icon'  => 'cubes',
            'url'   => $url.'/admin/system'
        ));

        $configMenu = $menu->createItem('sys-config', array(
            'label' => 'Configuration',
            'icon'  => 'wrench',
            'url'   => $url.'/admin/config'
        ));

        $terminalMenu = $menu->createItem('sys-debug', array(
            'label' => 'Debug',
            'icon'  => 'bar-chart',
            'url'   => $url.'/admin/dbg'
        ));

        $sysMenu->addChildren('sys-dashboard', $dashboardMenu);
        $sysMenu->addChildren('sys-config', $configMenu);
        $sysMenu->addChildren('sys-debug', $terminalMenu);

        $menu->addItem('sys', $sysMenu);
    }

    private function registerMiddlewares()
    {
        $c = $this->app->getContainer();
        $this->app->add(new CoreMiddleware($c));
    }

    private function registerViews()
    {
        // register template path & template alias
//        $viewLoader = $this->app->getContainer()->get('view')->getLoader();
//        $viewLoader->addPath(__DIR__ . '/Views', 'pages');
    }

    private function registerAdminRoute()
    {
        $this->app->group(self::getAdminUrl(), function () {
            $this->map(['GET', 'POST'], '', 'RunCMF\Admin\AdminController:index')->setName('admin');
            $this->map(['GET', 'POST'], '/system', 'RunCMF\Admin\AdminController:system');//->setName('admin/system');
            $this->map(['GET', 'POST'], '/config', 'RunCMF\Admin\AdminController:index');//->setName('admin/config');

            $this->group('/dbg', function () {
                $this->get('', 'RunCMF\Admin\Debug:index');
                $this->get('/trace', 'RunCMF\Admin\Debug:trace');
                $this->get('/code', 'RunCMF\Admin\Debug:code');
            });
        });
    }
}