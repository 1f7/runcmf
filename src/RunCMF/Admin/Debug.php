<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunCMF\Admin;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;
use Illuminate\Database\Capsule\Manager as DB;

class Debug extends AbstractController
{
    private $custom_namespace = 'Custom\\';

    public function __construct($c)
    {
        parent::__construct($c);

        $this->adm->init();
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('sys-debug');

        ini_set('xdebug.auto_trace', 'Off');
    }

    public function index(Request $request, Response $response)//, $args)
    {
//        $this->core->loadCss('/assets/css/highlight/railscasts.css', ['location' => 'external']);
//        $this->core->loadCss('/assets/runbb/cache/themes/theme2/css3.css', ['location' => 'external']);
//        $this->core->loadJs('highlight.pack.js');

//tdie($command);
//        $this->logger->info(__METHOD__ . ' action dispatched');

        $data = [

        ];

        $this->view->render($response, 'Admin/Debug.html.twig', $data);
    }

    public function trace(Request $request, Response $response) {

        $xdebug_trace_format = ini_get('xdebug.trace_format');
        $dir = ini_get('xdebug.trace_output_dir');
        if (!$dir) {
            $dir = '/tmp/';
        }
        $flist = '';
        $maxFileSize = 1024 * 1024;
        $files = new \DirectoryIterator($dir);

        foreach ($files as $file) {
            if ((substr_count($file->getFilename(), '.xt') == 0) ||
                ($file->getSize() > $maxFileSize))
            {
                continue;
            }
            $date = date('Y-m-d H:i:s', $file->getCTime());
            $flist .= '<option value="' . $file->getFilename() . '"> ' . $date . ' - ' . $file->getFilename() . '- ' . number_format($file->getSize() / 1024,
                    0,
                    ',',
                    '.') . ' KB</option>';
        }
        $traceFile = $dir . '/' . $request->getParam('file', '');
        $memJump = $request->getParam('memory', 0.3);
        $timeJump = $request->getParam('time', 0.03);

        if (!$request->getParam('file')) {
            $message = 'No file selected';
        } else if (!file_exists($traceFile)) {
            $message = 'No file exists';
        } else {
            $previousLevel = $ids = $jCnt = $lastTime = 0;
            $fullTrace = $levelIds = $aSumary = $aSumaryS = [];
            $defFn = get_defined_functions();
            $message = '';
            /**
             * Process all lines
             */
            $fh = fopen($traceFile, 'r');
            while ($jReadedLine = fgets($fh))
            {
                $jCnt++;
                $data = explode("\t", $jReadedLine);
                @list($level, $id, $point, $time, $memory, $function, $type, $file, $filename, $line, $numParms) = $data;

                switch ($xdebug_trace_format)
                {
                    /**
                     * @link https://xdebug.org/docs/execution_trace#trace_format
                     *
                     * xdebug.trace_format = 0 - shows a human readable
                     * xdebug.trace_format = 1 - computer readable format
                     * xdebug.trace_format = 2 - (simple) HTML
                     */
                    case 1:
//                        $data = explode("\t", $jReadedLine);
//                        @list($level, $id, $point, $time, $memory, $function, $type, $file, $filename, $line, $numParms) = $data;
                        /**
                         * if there is params save it
                         */
                        if (isset($numParms) and $numParms > 0) {
                            $valParms = '';
                            for ($i = 11; $i < (11 + $numParms); $i++) {
                                $valParms .= '<li>' . str_replace('\n', '<br />', htmlentities($data[$i])) . "</li>\n";
                            }
                        }
                        elseif (!empty($file)) {
                            $valParms = "<li>{$file}</li>";
                        } else {
                            $valParms = '';
                        }

                        $memory = round($memory / (1024 * 1024), 4);

                        break;

                    /**
                     * Other xdebug.trace_format
                     * @todo code for all types
                     */
                    default:
                        $strippedLine = preg_replace('(([\s]{2,}))', ' ', trim($line));
                        list ($time, $memory) = explode(" ", $strippedLine);
                        $memory = round($memory / (1024 * 1024), 4);
                        $level = round(((strpos($line, '->') - strpos($line, $memory) + strlen($memory))) / 2);
                        if ($level <= $previousLevel && isset($levelIds[$level])) {
                            $fullTrace[$levelIds[$level]]['timeOnExit'] = $time;
                            $fullTrace[$levelIds[$level]]['memoryOnExit'] = $memory;
                        }
                        $id = ++$ids;
                        $levelIds[$level] = $id;
                        $previousLevel = $level;

                        $parts = array_map('trim', explode("->", $line, 2));
                        $parts = explode(" ", $parts[1]);
                        $function = $parts[0];
                        list($line, $file) = array_map('strrev',
                            explode(":",
                                strrev($parts[1]),
                                2));
                        $filename = $file;
                        $point = 0;
                        $type = in_array(substr($function, 0,
                            strpos($function, "(")),
                            $defFn['internal']) ? 0 : 1;

                        $valParms = '';

                        break;
                }

                if (empty($function)) {
                    $fullTrace[$id]['timeOnExit'] = $time;
                    $fullTrace[$id]['memoryOnExit'] = $memory;
                    continue;
                }

                if ($point == 0) {
                    // starting function
                    $fullTrace[$id] = ['level' => $level,
                        'id' => $id,
                        'timeOnEntry' => $time,
                        'memoryOnEntry' => $memory,
                        'function' => $function,
                        'type' => $type,
                        'file' => $file,
                        'filename' => $filename,
                        'line' => $line,
                        'valParms' => $valParms
                    ];

                    if (isset($lastMemory) and ($memory - $lastMemory) > $memJump) {
                        $fullTrace[$id]['memoryAlert'] = $memory - $lastMemory;
                    } else {
                        $fullTrace[$id]['memoryAlert'] = false;
                    }

                    if (isset($lastMemory) and ($time - $lastTime) > $timeJump) {
                        $fullTrace[$id]['timeAlert'] = $time - $lastTime;
                    } else {
                        $fullTrace[$id]['timeAlert'] = false;
                    }

                    $lastMemory = $memory;
                    $lastTime = $time;
                } else {
                    $fullTrace[$id]['timeOnExit'] = $time;
                    $fullTrace[$id]['memoryOnExit'] = $memory;
                }
            }

            foreach ($fullTrace as $trace)
            {
                if (isset($trace['timeOnEntry']) and isset($trace['timeOnExit'])) {
                    $timeConsumed = ($trace['timeOnExit'] - $trace['timeOnEntry']) * 1000000;
                } else {
                    $timeConsumed = 0;
                }

                if (isset($trace['memoryOnEntry']) and isset($trace['memoryOnExit'])) {
                    $memoryConsumed = ($trace['memoryOnExit'] - $trace['memoryOnEntry']) * 1000000;
                } else {
                    $memoryConsumed = 0;
                }
                // calculate by functions
                if (isset($trace) and isset($trace['function']))
                {
                    if (isset($aSumary[$trace['function']])) {
                        $aSumary[$trace['function']]['mem'] += $memoryConsumed;
                        $aSumary[$trace['function']]['tim'] += $timeConsumed;
                        $aSumary[$trace['function']]['cnt']++;
                    } else {
                        $aSumary[$trace['function']]['func'] = $trace['function'];
                        $aSumary[$trace['function']]['mem'] = $memoryConsumed;
                        $aSumary[$trace['function']]['tim'] = $timeConsumed;
                        $aSumary[$trace['function']]['cnt'] = 1;
                    }
                }
                // calculate by filenames
                if (isset($trace) and isset($trace['filename']))
                {
                    if (isset($aSumaryS[$trace['filename']])) {
                        $aSumaryS[$trace['filename']]['mem'] += $memoryConsumed;
                        $aSumaryS[$trace['filename']]['tim'] += $timeConsumed;
                        $aSumaryS[$trace['filename']]['cnt']++;
                    } else {
                        $aSumaryS[$trace['filename']]['filename'] = $trace['filename'];
                        $aSumaryS[$trace['filename']]['mem'] = $memoryConsumed;
                        $aSumaryS[$trace['filename']]['tim'] = $timeConsumed;
                        $aSumaryS[$trace['filename']]['cnt'] = 1;
                    }
                }
            }
        }
        $this->usortByArrayKey($aSumary, 'tim', SORT_DESC);
        $this->usortByArrayKey($aSumaryS, 'tim', SORT_DESC);

        $data = [
            'flashMessage' => (!empty($message)) ? $message : '',
            'formPath' => 'trace',
            'logDirectory' => $dir,
            'traceFormat' => $xdebug_trace_format,
            'xtFilesList' => $flist,
            'traceFile' => $traceFile,
            'fullTrace' => (!empty($fullTrace)) ? $fullTrace : null,
            'tracesCount' => (!empty($fullTrace)) ? count($fullTrace) : 0,
            'll' => (!empty($fullTrace)) ? end($fullTrace) : '',
            'custom_namespace_len' => strlen($this->custom_namespace),
            'custom_namespace' => $this->custom_namespace,
            'summ_func' => $aSumary,
            'summ_file' => $aSumaryS,
            'memory_trigger' => $memJump,
            'time_trigger' => $timeJump
        ];
        unset($fullTrace);
//cl('test message');
        $this->view->render($response, 'Admin/DebugTrace.html.twig', $data);
    }

    public function graph(Request $request, Response $response) {
        @session_start();

        $xdb = new TraceGraph();
        $xdb->setParams();
//tdie($_GET['file']);
        $traceResult = $file = $filesize = '';
        if (isset($_GET['file'])) {
            $traceResult = $xdb->trace();
            $file = $xdb->file;
            $filesize = number_format($xdb->filesize, 0);
        }

        $data = [
            'formPath' => 'graph',
            'logDirectory' => $xdb->logDirectory,
            'traceFormat' => $xdb->traceFormat,
            'xtFilesList' => $xdb->rtvFiles(),
            'onlyOneInstruction' => $xdb->onlyOneInstruction,
            'onlyOneScript' => $xdb->onlyOneScript,
            'memoryAlarm' => $xdb->memoryAlarm,
            'timeAlarm' => $xdb->timeAlarm,
            'traceResult' => $traceResult,
            'file' => $file,
            'filesize' => $filesize,
        ];

        $this->view->render($response, 'Admin/DebugTrace.html.twig', $data);
    }

    public function code(Request $request, Response $response) {
        $file = $request->getParam('file');
        if (!$file || !file_exists($file)) {
            die('File not found...');
        }
        $line = $request->getParam('line');
        $lines = explode("\n", file_get_contents($file));
        $i = 1;
        $data = [];
        foreach ($lines as $cureLine) {
            $class = '';
            if ($i > ($line - 4) && $i < $line) {
                $class = 'near';
            }
            if ($i < ($line + 4) && $i > $line) {
                $class = 'near';
            }
            if ($i == $line) {
                $class = 'line';
            }
            $data[] = [
                'class' => $class,
                'num' => $i,
                'line' => str_replace('&lt;?php&nbsp;', '', highlight_string('<?php ' . $cureLine, true)),
            ];
            $i++;
        }


        $this->view->render($response, 'Admin/DebugTraceCode.html.twig', ['hline' => $data]);
    }

    function aryComp($a, $b) {
        if ($a['cnt'] == $b['cnt']) {
            return 0;
        }
        return ($a['cnt'] > $b['cnt']) ? -1 : 1;
    }

    function usortByArrayKey(&$array, $key, $asc=SORT_ASC) {
        if(!$array) {
            return;
        }
        $sort_flags = array(SORT_ASC, SORT_DESC);
        if(!in_array($asc, $sort_flags)) throw new \InvalidArgumentException('sort flag only accepts SORT_ASC or SORT_DESC');
        $cmp = function(array $a, array $b) use ($key, $asc, $sort_flags) {
            if(!is_array($key)) { //just one key and sort direction
                if(!isset($a[$key]) || !isset($b[$key])) {
                    throw new \Exception('attempting to sort on non-existent keys');
                }
                if($a[$key] == $b[$key]) return 0;
                return ($asc==SORT_ASC xor $a[$key] < $b[$key]) ? 1 : -1;
            } else { //using multiple keys for sort and sub-sort
                foreach($key as $sub_key => $sub_asc) {
                    //array can come as 'sort_key'=>SORT_ASC|SORT_DESC or just 'sort_key', so need to detect which
                    if(!in_array($sub_asc, $sort_flags)) { $sub_key = $sub_asc; $sub_asc = $asc; }
                    //just like above, except 'continue' in place of return 0
                    if(!isset($a[$sub_key]) || !isset($b[$sub_key])) {
                        throw new \Exception('attempting to sort on non-existent keys');
                    }
                    if($a[$sub_key] == $b[$sub_key]) continue;
                    return ($sub_asc==SORT_ASC xor $a[$sub_key] < $b[$sub_key]) ? 1 : -1;
                }
                return 0;
            }
        };
        usort($array, $cmp);
    }
}