<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunCMF\Admin;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RunCMF\Core\AbstractController;
use Illuminate\Database\Capsule\Manager as DB;

class AdminController extends AbstractController
{

    public function index(Request $request, Response $response, $args)
    {
        $this->adm->init();
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('dashboard');

        $this->logger->info(__METHOD__ . ' action dispatched');

        $data['test'] = 'bla-bla';

        $this->view->render($response, 'Admin/Index.html.twig', $data);
    }

    public function system(Request $request, Response $response, $args)
    {
        $this->adm->init();
        $this->bb->menu->get('admin_sidebar')->setActiveMenu('sys-dashboard');

        $this->logger->info(__METHOD__ . ' action dispatched');

        $data['test'] = 'bla-bla';

        $this->view->render($response, 'Admin/System.html.twig', $data);
    }
}
