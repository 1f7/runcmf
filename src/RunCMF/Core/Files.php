<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunCMF\Core;

use ErrorException;
use RunCMF\Exceptions\FileNotFoundException;

class Files extends AbstractController
{
    public function isDirectory($dir)
    {
        return is_dir($dir);
    }

    public function isWritable($path)
    {
        return is_writable($path);
    }

    public function isFile($file)
    {
        return is_file($file);
    }

    public function glob($pattern, $flags = 0)
    {
        return glob($pattern, $flags);
    }

    public function exists($file)
    {
        return file_exists($file);
    }

    public function size($file)
    {
        return filesize($file);
    }

    public function extension($path)
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }

    public function dirname($path)
    {
        return pathinfo($path, PATHINFO_DIRNAME);
    }

    public function basename($path)
    {
        return pathinfo($path, PATHINFO_BASENAME);
    }

    public function name($path)
    {
        return pathinfo($path, PATHINFO_FILENAME);
    }

    public function copy($from, $to)
    {
        return copy($from, $to);
    }

    public function move($from, $to)
    {
        return rename($from, $to);
    }

    public function get($file, $lock = false)
    {
        if ($this->isFile($file)) {
            return $lock ? $this->sharedGet($file) : file_get_contents($file);
        }
        throw  new FileNotFoundException('File not found in: ' . $file);
    }

    public function put($path, $content, $lock = false)
    {
        return file_put_contents($path, $content, $lock ? LOCK_EX : 0);
    }

    public function prepend($path, $data)
    {
        if ($this->exists($path)) {
            return $this->put($path, $data . $this->get($path));
        }
        return $this->put($path, $data);
    }

    public function append($path, $data)
    {
        return file_put_contents($path, $data, FILE_APPEND);
    }

    public function sharedGet($path)
    {
        $content = '';
        $fh = fopen($path, 'rb');
        if ($fh) {
            try {
                if (flock($fh, LOCK_SH)) {
                    clearstatcache(true, $path);
                    $content = fread($fh, $this->size($path) ?: 1);
                    flock($fh, LOCK_UN);
                }
            } finally {
                fclose($fh);
            }
        }
        return $content;
    }

    public function delete($paths)
    {
        $paths = is_array($paths) ? $paths : func_get_args();
        $result = true;
        foreach ($paths as $path) {
            try {
                if (!@unlink($path)) {
                    $result = false;
                }
            } catch (ErrorException $e) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Read all files or all subdirs in geven directory
     *
     * @param $path string given directory
     * @param $flat boolean flat array or multidimensional
     * @param $dir_only boolean collect only directory
     * @param $exclude string separated by | additional list to exclude array
     *
     * @return array $result Files List Array
     */
    public function getList($path = '.', $flat = true, $dir_only = false, $exclude = '')
    {
        $path = rtrim($path, '/') . '/';
        $result = [];
        if (!is_dir($path)) {
            return $result[] = 'ERROR: Given at: ' . $path . ', is not directory';
        }
        $exclude_arry = ['.', '..', 'CVS', '.svn', '.git'];
        $add_exclude = explode('|', $exclude);
        if (count($add_exclude) > 1) {
            $exclude_arry = array_merge($exclude_arry, $add_exclude);
        }
        if (!$dir_handle = opendir($path)) {
            return $result[] = 'ERROR: Can Not Open: ' . $path . '.';
        }
        while (false !== ($file = readdir($dir_handle))) {
            if (!in_array($file, $exclude_arry)) {
                if ($flat) {
                    if (is_dir($path . $file . '/')) {
                        $result = array_merge($result, $this->getList($path . $file . '/', $flat, $dir_only, $exclude));
                    } elseif (!$dir_only) {
                        $result[] = array('path' => $path, 'file' => $file);
                    } else {
                        $result[] = $path;
                    }
                } else {
                    if (is_dir($path . $file . '/')) {
                        $result[][$file] = $this->getList($path . $file . '/', $flat, $dir_only, $exclude);
                    } elseif (!$dir_only) {
                        $result[] = $path . $file;
                    }
                }
            }
        }
        if ($flat && $dir_only) {
            $result = array_unique($result);
        }
        closedir($dir_handle);
        return $result;
    }
}