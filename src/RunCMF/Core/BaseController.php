<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunCMF\Core;

use Slim\Container;

class BaseController extends AbstractController
{
    public $data;

    public function __construct(Container $c)
    {
        parent::__construct($c);

        $this->data = [
            'title' => 'Dashboard',
            'meta' => [],
            'css' => [
                'internal'  => [],
                'external'  => []
            ],
            'js' => [
                'internal'  => [],
                'external'  => []
            ],
            'message' => [
                'error' => [],
                'info' => [],
                'debug' => []
            ],
            'cnt' => new \stdClass(),
            'global' => [],
//            'baseUrl' => '/',
//            'assetUrl' => '/assets/'
        ];

        $this->data['cnt']->categories = 7;
        $this->data['cnt']->pages = 44;
        $this->data['cnt']->users = 22;
        $this->data['cnt']->options = 333;

//        $this->data['baseUrl']  = $this->baseUrl();
//        $this->data['assetUrl'] = $this->data['baseUrl'].'/assets/';

        $this->loadBaseCss();
        $this->loadBaseJs();
//        $this->buildUserMenu();// initi in BB

//        $this->view->offsetAdd($this->data);// move to AdminCommon.php
    }

    public function buildUserMenu()
    {
        $m = '';
        foreach ($this->settings['modules'] as $name => $module) {
            $m .= $this->$name->registerUserMenu();
        }

        return $m;
    }

    public function buildSidebarMenu()
    {
//        $this->data['menu_pointer'] = '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';

        $adminMenu = $this->menu->create('admin_sidebar');
        $dashboard = $adminMenu->createItem('dashboard', [
            'label' => 'Dashboard',
            'icon'  => 'dashboard',
            'url'   => $this->settings['siteUrl'].'/admin'
        ]);

        $adminMenu->addItem('dashboard', $dashboard);

        foreach ($this->settings['modules'] as $module) {
            $module::registerAdminMenu($adminMenu, $this->settings['siteUrl']);
        }
    }

    /**
     * enqueue css asset to be loaded
     * @param  [string] $css     [css file to be loaded relative to base_asset_dir]
     * @param  [array]  $options [location=internal|external, position=first|last|after:file|before:file]
     */
    public function loadCss($css, $options=array())
    {
        $location = (isset($options['location'])) ? $options['location']:'internal';

        //after:file, before:file, first, last
        $position = (isset($options['position'])) ? $options['position']:'last';

        if(!in_array($css,$this->data['css'][$location])){
            if($position=='first' || $position=='last'){
                $key = $position;
                $file='';
            }else{
                list($key,$file) =  explode(':',$position);
            }

            switch($key){
                case 'first':
                    array_unshift($this->data['css'][$location],$css);
                    break;

                case 'last':
                    $this->data['css'][$location][]=$css;
                    break;

                case 'before':
                case 'after':
                    $varkey = array_keys($this->data['css'][$location],$file);
                    if($varkey){
                        $nextkey = ($key=='after') ? $varkey[0]+1 : $varkey[0];
                        array_splice($this->data['css'][$location],$nextkey,0,$css);
                    }else{
                        $this->data['css'][$location][]=$css;
                    }
                    break;
            }
        }
    }


    /**
     * enqueue js asset to be loaded
     * @param  [string] $js      [js file to be loaded relative to base_asset_dir]
     * @param  [array]  $options [location=internal|external, position=first|last|after:file|before:file]
     */
    public function loadJs($js, $options=array())
    {
        $location = (isset($options['location'])) ? $options['location']:'internal';

        //after:file, before:file, first, last
        $position = (isset($options['position'])) ? $options['position']:'last';

        if(!in_array($js,$this->data['js'][$location])){
            if($position=='first' || $position=='last'){
                $key = $position;
                $file='';
            }else{
                list($key,$file) =  explode(':',$position);
            }

            switch($key){
                case 'first':
                    array_unshift($this->data['js'][$location],$js);
                    break;

                case 'last':
                    $this->data['js'][$location][]=$js;
                    break;

                case 'before':
                case 'after':
                    $varkey = array_keys($this->data['js'][$location],$file);
                    if($varkey){
                        $nextkey = ($key=='after') ? $varkey[0]+1 : $varkey[0];
                        array_splice($this->data['js'][$location],$nextkey,0,$js);
                    }else{
                        $this->data['js'][$location][]=$js;
                    }
                    break;
            }
        }
    }

    /**
     * clear enqueued css asset
     */
    public function resetCss()
    {
        $this->data['css'] = [
            'internal'  => [],
            'external'  => []
        ];
    }

    /**
     * clear enqueued js asset
     */
    public function resetJs()
    {
        $this->data['js'] = [
            'internal'  => [],
            'external'  => []
        ];
    }

    /**
     * remove individual css file from queue list
     * @param  [string] $css [css file to be removed]
     */
    public function removeCss($css)
    {
        $key=array_keys($this->data['css']['internal'],$css);
        if($key){
            array_splice($this->data['css']['internal'],$key[0],1);
        }

        $key=array_keys($this->data['css']['external'],$css);
        if($key){
            array_splice($this->data['css']['external'],$key[0],1);
        }
    }

    /**
     * remove individual js file from queue list
     * @param  [string] $js [js file to be removed]
     */
    public function removeJs($js)
    {
        $key=array_keys($this->data['js']['internal'],$js);
        if($key){
            array_splice($this->data['js']['internal'],$key[0],1);
        }

        $key=array_keys($this->data['js']['external'],$js);
        if($key){
            array_splice($this->data['js']['external'],$key[0],1);
        }
    }

    /**
     * addMessage to be viewd in the view file
     */
    public function message($message, $type='info')
    {
        $this->data['message'][$type] = $message;
    }

    /**
     * register global variable to be accessed via javascript
     */
    public function publish($key,$val)
    {
        $this->data['global'][$key] =  $val;
    }

    /**
     * remove published variable from registry
     */
    protected function unpublish($key)
    {
        unset($this->data['global'][$key]);
    }

    /**
     * add custom meta tags to the page
     */
    public function meta($name, $content)
    {
        $this->data['meta'][$name] = $content;
    }

    /**
     * load base css for the template
     */
    protected function loadBaseCss()
    {
//        $this->loadCss("bootstrap.min.css");
//        $this->loadCss("font-awesome.min.css");
//        $this->loadCss("sb-admin.css");
//        $this->loadCss("custom.css");
    }

    /**
     * load base js for the template
     */
    protected function loadBaseJs()
    {
//        $this->loadJs("jquery-1.10.2.js");
//        $this->loadJs("bootstrap.min.js");
//        $this->loadJs('metisMenu.min.js');
//        $this->loadJs('sb-admin.js');
    }
}