<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunCMF\Core;

class Modules extends AbstractController
{
    public function __construct($c)
    {
        parent::__construct($c);
        include __DIR__ .'/../shortcuts.php';
    }

    public function initModules(& $app, array $modules = [])
    {
        if(count($modules) > 0) {
            foreach ($modules as $name => $mod) {
//                (new $mod($app))->init();
                $this->$name = new $mod($app);
                $this->$name->init();
            }
        }
    }

    public function getUserMenu()
    {
        $m = '';
        foreach ($this->settings['modules'] as $name => $class) {
            $m .= $this->$name->registerUserMenu();
        }

        return $m;
    }
}