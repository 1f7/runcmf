<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace RunCMF\Core\Menu;

class TwigMenuRenderer extends \Twig_Extension
{
    private $sc;// Slim Container

    public function __construct($c)
    {
        $this->sc = $c;
    }

    public function getName()
    {
        return 'menu_renderer';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('render_menu', [$this, 'renderMenu'])
        );
    }

    public function renderMenu($name, $tag = 'ul', $option)
    {
        return $this->sc->get('menu')->render($name, $tag, $option);
    }
}