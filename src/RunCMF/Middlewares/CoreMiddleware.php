<?php
/**
 * Copyright 2016 1f7.wizard@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


namespace RunCMF\Middlewares;

use RunCMF\Core\BaseController;
use RunCMF\Core\Menu\TwigMenuRenderer;
use RunCMF\Core\Menu\MenuManager;
use RunCMF\Core\Files;

class CoreMiddleware
{
    private $c;

    public function __construct(& $c)
    {
        $this->c = $c;
    }

    public function __invoke($request, $response, $next)
    {
//        $c = $this->app->getContainer();
        $this->c['core'] = function ($c) {
            return new BaseController($c);
        };
        $this->c['menu'] = function ($c) {
            return new MenuManager($c);
        };
        $this->c['files'] = function ($c) {
            return new Files($c);
        };

        // load twig helper
//        $this->c['view']->addExtension(new TwigMenuRenderer($this->c));

        // Add some global wars
//        $c->get('view')->offsetSet('siteUrl', $c->get('settings')['siteUrl']);
//        $c->get('view')->offsetSet('assetUrl', $c->get('settings')['siteUrl'] . '/assets/');

//        $this->c['bb']->initBB();


        return $next($request, $response);
    }
}